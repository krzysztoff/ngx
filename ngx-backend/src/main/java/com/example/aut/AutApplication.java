package com.example.aut;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutApplication {

    public static void main(String[] args) {
        SpringApplication.run(AutApplication.class, args);
    }

}
