package com.example.aut;

import java.util.Date;

public class Token {
    Date createdAt;
    String value;
    boolean valid;

    public Token() {
        this.createdAt = new Date();
        this.value = "";
        this.valid = true;
    }

    public Token(Date createdAt, String value, boolean valid) {
        this.createdAt = createdAt;
        this.value = value;
        this.valid = valid;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    @Override
    public String toString() {
        return "Token{" +
                "createdAt=" + createdAt +
                ", value='" + value + '\'' +
                ", valid=" + valid +
                '}';
    }
}
